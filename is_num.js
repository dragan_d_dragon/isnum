//Should be self explanatory
var createIsNum = function() {
	
	var self = Object.create(Object);

	self.isAnyNum = function(candidate){
		var res = (candidate / 1);
		return  res === res;
	}

	self.isReal = function(candidate){
		var res = candidate / 1;
		return  self.isAnyNum(candidate) && (res !== Infinity) && (res !== -Infinity);
	}

	self.isInt = function(candidate){
		var candidate = parseInt(candidate);
		return self.isReal(candidate) &&  ((candidate - candidate % 1) === candidate); 
	}

	self.isRealOnly = function(candidate){
		var candidate = parseFloat(candidate);
		return self.isReal(candidate) && ((candidate - candidate % 1) !== candidate);
	}
	return self;
}

var isnn = createIsNum();